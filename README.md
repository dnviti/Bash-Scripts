# RHEL Post-Install

Git Repository: <u>*[RHEL Post Install](https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh)*</u>

<details id="bkmrk-docker-standalone-st"><summary>Docker Standalone</summary>

Standard Installation with prerequisites

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -do" root
```

Standard Installation with prerequisites (with QEMU Guest Tools)

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -do -qa" root
```

Standard Installation with prerequisites (force manual IP)

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo nmcli connection modify ens3 IPv4.method manual && \
sudo nmcli connection down ens3 && sudo nmcli connection up ens3 && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -do" root
```

</details><details id="bkmrk-docker-%2B-minikube-st"><summary>Docker + Minikube</summary>

Standard Installation with prerequisites

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -mk" root
```

Standard Installation with prerequisites (force manual IP)

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo nmcli connection modify ens3 IPv4.method manual && \
sudo nmcli connection down ens3 && sudo nmcli connection up ens3 && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -mk" root
```

</details><details id="bkmrk-kubernetes-install-k"><summary>K3S Master</summary>

Standard Installation with prerequisites and portainer

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -km -pt" root
```

Standard Installation with prerequisites and portainer (force manual IP)

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo nmcli connection modify ens3 IPv4.method manual && \
sudo nmcli connection down ens3 && sudo nmcli connection up ens3 && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -km -pt" root
```

</details><details id="bkmrk-kubernetes-node-stan"><summary>K3S Node</summary>

Standard Installation with prerequisites

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -kn" root
```

Standard Installation with prerequisites and portainer (force manual IP)

```bash
echo "echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" | sudo su && \
sudo nmcli connection modify ens3 IPv4.method manual && \
sudo nmcli connection down ens3 && sudo nmcli connection up ens3 && \
sudo dnf install nano wget -y && \
sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -kn" root
```

</details>### Help

```
RHEL Docker + Kubernetes Post-Install usage
----
Docker/Kubernetes Installer:
  -do | --docker-only) -> Install Docker Only
  -mk | --minikube   ) -> Install Minikube
  -km | --kube-master) -> Install Kubernetes Master
  -kn | --kube-node  ) -> Install Kubernetes Node
----
Optional Parameters:
  -u    | --user)              -> Specify non-root user to run the script
  -a    | --gateway-address)   -> Specify a network gateway address to be used for firewalling purposes
  -dd   | --docker-directory)  -> Specify a custom docker directory
  -hs   | --hostname-change)   -> Specify a new machine hostname
  -ni   | --network-interface) -> Specify the main Network Interface name
  -fwd  | --enable-firewall)   -> Enable firewall (disabled by default)
  -dhcp | --force-dhcp-renew)  -> Force DHCP refresh
  -ipa  | --manual-ip)         -> Specify manual network addresses (IP/mask, Gateway, DNS)
  -sp   | --change-ssh-port)   -> Change default SSH port
  -nu   | --not-auto-update)   -> Do not install AUTOUPD and upgrade system with default method
  -us   | --auto-sec-upd)      -> Automatically upgrade only security
  -hd   | --headless)          -> Do not ask for reboot at the end (for automatic provisioners like Vagrant)
  -qa   | --qemu-agent)        -> Install Quemu Agent drivers for KVM based Hypervisors
  -pt   | --portainer)         -> Install Portainer Docker Manager
  -si   | --docker-swarm-init) -> Initialize a new Docker Swarm Master
  -sj   | --docker-swarm-join) -> Provide a docker swarm key to connect this node to a swarm master
```

### Run script from WGET

```shell
# Non-Interactive Scripts
wget -O - http://website.com/my-script.sh | bash

# Note that when using this method, interactive scripts will not work.
# Interactive Scripts
# In order to get interactive scripts working, you can use this:
bash <(wget -qO- http://website.com/my-script.sh)

# Interactive Scripts that need to be run as root
# Warning: This is extremely dangerous. Not recommended.
sudo su -c "bash <(wget -qO- http://website.com/my-script.sh)" root

# Note that you cannot simple use sudo bash since using <(...) will
# create a virtual file and it's file descriptor will not be accessible
# from roots bash instance. It must be executed on the same user that needs to
# read the virtual file, so it has to be sent as it's own command inside the root users shell.
```

### External Reference

> By default, the directory where the Fail2ban configuration files reside is `/etc/fail2ban/` and there will be two files that we do not have to modify, `/etc/fail2ban/jail.conf` and `/etc/fail2ban/jail.d/00-firewalld.conf`.
> 
> Therefore, the recommended way to make configurations is to copy the entire contents of `jail.conf` into a file called `jail.local` in the same directory. This is because the `.local` files will overwrite the `.conf` files. It is also possible to create the file from the scratch.
> 
> Whether you copy the contents of `jail.conf` or start from scratch you can add your own settings.
> 
> For example, some basic configurations to do with Fail2ban can be:
> 
> - Bantime: Time in seconds that the IP will be banned.
> - Maxretry: Number of retries allowed before being banned.
> - Findtime: If the host makes the `maxretry` in the amount of time expressed in `findtime`, then it will be banned.
> - Banaction: Action that the system will do when banning the host.
> - Backend: Where fail2ban logs are taken from.
> 
> Also with the `ignoreip` value you can define an IP address or a range that Fail2ban will ignore.
