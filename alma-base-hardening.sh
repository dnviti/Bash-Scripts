#!/bin/bash

#set -o xtrace

if [[ $USER != "root" ]]
then
    echo "Script must be run as root"
    exit 1
fi

sed -i "s/#Port/Port/g" /etc/ssh/sshd_config
S=$(grep "Port " /etc/ssh/sshd_config)
OLD_SSH_PORT=${S:5:99}
NEW_SSH_PORT=$(( $RANDOM % 999 + 22000 ))
SSH_GW_IP=$(ip route | grep default | awk '{print $3}' | awk 'END{print}')
## Use the first sudoer user available post-install
SCRIPT_USER=$(id -nu 1000)


### START SCRIPT

### Force local time with NTP
sudo timedatectl set-ntp true
timedatectl
sudo dnf install chrony -y
sudo systemctl enable chronyd
sudo systemctl restart chronyd
date

### FORCE FIREWALL ON
sudo dnf install firewalld -y
sudo systemctl enable --now firewalld

HOSTNAME=$(cat /etc/hostname)
echo "Hostname is set to: $HOSTNAME"


## System Utilities Install
sudo dnf install -y nano ca-certificates curl epel-release policycoreutils-python-utils dhclient
sudo dnf install -y fail2ban fail2ban-firewalld

## Security Prep
## SSH Config

  echo "Changing SSH port..."
  sudo sed -i "s/Port $OLD_SSH_PORT/Port $NEW_SSH_PORT/g" /etc/ssh/sshd_config
  sudo sed -i "s/PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config

  echo "Changing Firewall rules according to the new SSH Port"
  sudo firewall-cmd --permanent --zone=public --add-port=$NEW_SSH_PORT/tcp
  sudo firewall-cmd --permanent --zone=public --add-source=$NETWORK_IP/24
  sudo firewall-cmd --remove-service=ssh --zone=public --permanent
  sudo firewall-cmd --remove-service=cockpit --zone=public --permanent
  sudo firewall-cmd --remove-service=dhcpv6-client --zone=public --permanent

  echo "Setting Selinux for SSH Port..."
  sudo semanage port -a -t ssh_port_t -p tcp $NEW_SSH_PORT


sudo systemctl enable --now fail2ban
sudo tee -a /etc/fail2ban/jail.conf > /dev/null <<EOT

[ssh]
enabled = true
port = $NEW_SSH_PORT
filter = sshd
logpath = /var/log/auth.log
EOT


  echo "Found AUTOUPD for security system upgrade"
  sudo dnf install dnf-automatic -y
  sed -i "s/upgrade_type = default/upgrade_type = security/g" /etc/dnf/automatic.conf
  sed -i "s/download_updates = no/download_updates = yes/g" /etc/dnf/automatic.conf
  sed -i "s/apply_updates = no/apply_updates = yes/g" /etc/dnf/automatic.conf
  sudo systemctl enable --now dnf-automatic.timer



  NEW_SCRIPT_USER_PASSWORD=$(tr -cd "[:alnum:]" < /dev/urandom | fold -w32 | head -n 1)
  NEW_ROOT_PASSWORD=$(tr -cd "[:alnum:]" < /dev/urandom | fold -w32 | head -n 1)

  ## User Output
  echo "Setting secure password for user $SCRIPT_USER"
  echo "$SCRIPT_USER:$NEW_SCRIPT_USER_PASSWORD"|chpasswd
  echo "Setting secure password for user root"
  echo "root:$NEW_ROOT_PASSWORD"|chpasswd

  echo "-"
  echo "-"
  echo "-"
  echo "Script end - User Information"


    echo "New $SCRIPT_USER password is: $NEW_SCRIPT_USER_PASSWORD"
    echo "New root password is: $NEW_ROOT_PASSWORD"
    echo "Connect via SSH: ssh -p $NEW_SSH_PORT $SCRIPT_USER@$MACHINE_IP"
    echo "Warning! System will be restarted, please save all the info before"
    read -p "Press [Enter] to reboot system"
    reboot


echo "Automatic provisioning completed, you may want to restart bash session"