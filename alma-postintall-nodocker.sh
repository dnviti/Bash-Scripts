#!/bin/bash

#set -o xtrace

if [[ $USER != "root" ]]
then
    echo "Script must be run as root"
    exit 1
fi

sed -i "s/#Port/Port/g" /etc/ssh/sshd_config
S=$(grep "Port " /etc/ssh/sshd_config)
OLD_SSH_PORT=${S:5:99}
NEW_SSH_PORT=$(( $RANDOM % 999 + 22000 ))
SSH_GW_IP=$(ip route | grep default | awk '{print $3}' | awk 'END{print}')
## Use the first sudoer user available post-install
SCRIPT_USER=$(id -nu 1000)
## by default script will not install kubernetes
KUBE_INSTALL="n"
KUBE_MASTER="n"
NETINTERFACE=$(ip route | grep default | awk '{print $5}' | awk 'END{print}')
MACHINE_IP=$(ip route | grep default | awk '{print $9}' | awk 'END{print}')
MACHINE_GATEWAY=$SSH_GW_IP
MACHINE_DNS=""
DOCKER_DIRECTORY="/docker"
DHCP_FORCE="n"
SSH_CHANGE_PORT="n"
MANUAL_IP="n"
AUTOUPD="y"
UPDSEC="y"
HEADLESS="n"
QEMUAGENT="n"
SWARM_MASTER="n"
PORTAINER="n"
SWARM_KEY=""
CREATE_USER_CHOICE="y"

# if no arguments are provided, return usage function
# maybe no need
# if [ $# -eq 0 ]; then
#     usage # run usage function
#     exit 1
# fi

usage () {
  echo "RHEL Docker + Kubernetes Post-Install usage"
  echo "     -u    | --user)              -> Specify non-root user to run the script"
  echo "     -a    | --gateway-address)   -> Specify a network gateway address to be used for firewalling purposes"
  echo "     -hs   | --hostname-change)   -> Specify a new machine hostname"
  echo "     -ni   | --network-interface) -> Specify the main Network Interface name"
  echo "     -fwd  | --enable-firewall)   -> Enable firewall (disabled by default)"
  echo "     -dhcp | --force-dhcp-renew)  -> Force DHCP refresh"
  echo "     -ipa  | --manual-ip)         -> Specify manual network addresses (IP/mask, Gateway, DNS)"
  echo "     -sp   | --change-ssh-port)   -> Change default SSH port"
  echo "     -us   | --auto-sec-upd)      -> Automatically upgrade only security"
  echo "     -hd   | --headless)          -> Do not ask for reboot at the end (for automatic provisioners like Vagrant)"
  echo "----"

  echo "Release Date: 25/06/2023"
  echo "Author: pietro.broccolo@commandware.com"
}

while [ "$1" != "" ]; do
    case $1 in
    -hs | --hostname-change)
        shift
        HOSTNAME=$1
        echo "$HOSTNAME" > /etc/hostname
        ;;
    -u | --user)
        shift
        ## Specify a manual Script user
        SCRIPT_USER=$1
        ## Script User Check
        if [[ $SCRIPT_USER == "" ]]
        then
          echo "No user defined"
          exit 1
        fi
        if id "$SCRIPT_USER" &>/dev/null; then
            USER_EXISTS="y"
        else
            USER_EXISTS="n"
        fi
        ;;
    -a | --gateway-address)
        shift
        ## Specify a manual SSH Gateway IP
        SSH_GW_IP=$1
        ;;
    -ni | --network-interface)
        shift
        NETINTERFACE=$1
        ;;
    -fwd | --enable-firewall)
        ENABLE_FIREWALL="y"
        ;;
    -dhcp | --force-dhcp-renew)
        DHCP_FORCE="y"
        ;;
    -ipa | --manual-ip)
        MANUAL_IP="y"
        ;;
    -sp | --change-ssh-port)
        SSH_CHANGE_PORT="y"
        ;;
    -hd | --headless)
        HEADLESS="y"
        ;;
    -qa | --qemu-agent)
        QEMUAGENT="y"
        ;;
    -h | --help)
        usage # run usage function
        ;;
    *)
        usage
        exit 1
        ;;
    esac
    shift # remove the current value for '$1' and use the next
done

### START SCRIPT

### Force local time with NTP
sudo timedatectl set-ntp true
timedatectl
sudo dnf install chrony -y
sudo systemctl enable chronyd
sudo systemctl restart chronyd
date

### FORCE FIREWALL ON
sudo dnf install firewalld -y
sudo systemctl enable --now firewalld


### Install QUEMU Agent
if [[ $QEMUAGENT = "y" ]]
then
  sudo dnf install qemu-guest-agent -y
  sudo systemctl enable qemu-guest-agent
  sudo systemctl start qemu-guest-agent
fi

HOSTNAME=$(cat /etc/hostname)
echo "Hostname is set to: $HOSTNAME"

if [[ $SSH_GW_IP == "" ]]
then
  echo "You must enter the Gateway IP to allow connections"
  exit 1
fi

## Find network address from gateway
NETWORK_IP=$(awk -F"." '{print $1"."$2"."$3".2"}'<<<$SSH_GW_IP)

if [[ $SCRIPT_USER == "" ]]
then
  echo "You must enter a non root user as parameter"
	exit 1
fi

if id "$SCRIPT_USER" &>/dev/null; then
    USER_EXISTS="y"
else
    USER_EXISTS="n"
fi

## Replace current HOME variable with new one for script user selected
HOME="/home/$SCRIPT_USER"

## Adding the new user if not exists
if [[ $USER_EXISTS == "n" ]]
then
  if [[ $HEADLESS == "n" ]]
  then
    read -p "The User specified [$SCRIPT_USER] does not exists, should i create it? (N/y)" CREATE_USER_CHOICE
  else
    $CREATE_USER_CHOICE = "y"
  fi
  if [[ $CREATE_USER_CHOICE = "" || $CREATE_USER_CHOICE == "n" || $CREATE_USER_CHOICE == "N" ]]
  then
    echo "Error: User [$SCRIPT_USER] does not exists and you do not automatically want to create it. Aborting"
    exit 1
  fi
  echo "User $SCRIPT_USER does not exists, adding..."
  sudo adduser $SCRIPT_USER
  ## Create a new home and set permissions
  sudo mkdir "$HOME"
  sudo chown $SCRIPT_USER:$SCRIPT_USER "$HOME" -R
  sudo chmod u+rwx "$HOME" -R
  ## Add user to sudoers
  sudo usermod -aG sudo $SCRIPT_USER
fi

## System Utilities Install
sudo dnf install -y nano ca-certificates curl epel-release policycoreutils-python-utils dhclient
sudo dnf install -y fail2ban fail2ban-firewalld

## Security Prep
## SSH Config
if [[ $SSH_CHANGE_PORT = "y" ]]
then
  echo "Changing SSH port..."
  sudo sed -i "s/Port $OLD_SSH_PORT/Port $NEW_SSH_PORT/g" /etc/ssh/sshd_config
  sudo sed -i "s/PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config

  echo "Changing Firewall rules according to the new SSH Port"
  sudo firewall-cmd --permanent --zone=public --add-port=$NEW_SSH_PORT/tcp
  sudo firewall-cmd --permanent --zone=public --add-source=$NETWORK_IP/24
  sudo firewall-cmd --remove-service=ssh --zone=public --permanent
  sudo firewall-cmd --remove-service=cockpit --zone=public --permanent
  sudo firewall-cmd --remove-service=dhcpv6-client --zone=public --permanent

  echo "Setting Selinux for SSH Port..."
  sudo semanage port -a -t ssh_port_t -p tcp $NEW_SSH_PORT
else
  NEW_SSH_PORT="$OLD_SSH_PORT"
fi

sudo systemctl enable --now fail2ban
sudo tee -a /etc/fail2ban/jail.conf > /dev/null <<EOT

[ssh]
enabled = true
port = $NEW_SSH_PORT
filter = sshd
logpath = /var/log/auth.log
EOT

# Upgrade the entire system automatically
if [[ $AUTOUPD == "y" ]]
then
  echo "Found AUTOUPD for security system upgrade"
  sudo dnf install dnf-automatic -y
  if [[ $UPDSEC == "y" ]]
  then
    sed -i "s/upgrade_type = default/upgrade_type = security/g" /etc/dnf/automatic.conf
  fi
  sed -i "s/download_updates = no/download_updates = yes/g" /etc/dnf/automatic.conf
  sed -i "s/apply_updates = no/apply_updates = yes/g" /etc/dnf/automatic.conf
  sudo systemctl enable --now dnf-automatic.timer
  #docker run --detach --name watchtower -e WATCHTOWER_SCHEDULE="0 0 5 * * *" --restart unless-stopped --volume /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower
else
  echo "Running standard dnf upgrade"
  sudo dnf update -y
  sudo dnf autoremove -y
fi

if [[ $HEADLESS = "n" ]]
then
  NEW_SCRIPT_USER_PASSWORD=$(tr -cd "[:alnum:]" < /dev/urandom | fold -w32 | head -n 1)
  NEW_ROOT_PASSWORD=$(tr -cd "[:alnum:]" < /dev/urandom | fold -w32 | head -n 1)

  ## User Output
  echo "Setting secure password for user $SCRIPT_USER"
  echo "$SCRIPT_USER:$NEW_SCRIPT_USER_PASSWORD"|chpasswd
  echo "Setting secure password for user root"
  echo "root:$NEW_ROOT_PASSWORD"|chpasswd

  echo "-"
  echo "-"
  echo "-"
  echo "Script end - User Information"

  if [[ $HEADLESS = "n" ]]
  then
    echo "New $SCRIPT_USER password is: $NEW_SCRIPT_USER_PASSWORD"
    echo "New root password is: $NEW_ROOT_PASSWORD"
    echo "Connect via SSH: ssh -p $NEW_SSH_PORT $SCRIPT_USER@$MACHINE_IP"
    echo "Warning! System will be restarted, please save all the info before"
    read -p "Press [Enter] to reboot system"
    reboot
  fi
fi

echo "Automatic provisioning completed, you may want to restart bash session"