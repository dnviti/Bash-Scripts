#!/bin/bash

#set -o xtrace

if [[ $USER != "root" ]]
then
    echo "Script must be run as root"
    exit 1
fi

sed -i "s/#Port/Port/g" /etc/ssh/sshd_config
S=$(grep "Port " /etc/ssh/sshd_config)
OLD_SSH_PORT=${S:5:99}
NEW_SSH_PORT=$(( $RANDOM % 999 + 22000 ))
SSH_GW_IP=$(ip route | grep default | awk '{print $3}' | awk 'END{print}')
## Use the first sudoer user available post-install
SCRIPT_USER=$(id -nu 1000)
## by default script will not install kubernetes
KUBE_INSTALL="n"
KUBE_MASTER="n"
MINIKUBE="n"
NETINTERFACE=$(ip route | grep default | awk '{print $5}' | awk 'END{print}')
MACHINE_IP=$(ip route | grep default | awk '{print $9}' | awk 'END{print}')
MACHINE_GATEWAY=$SSH_GW_IP
MACHINE_DNS=""
DOCKER_DIRECTORY="/docker"
DHCP_FORCE="n"
SSH_CHANGE_PORT="n"
MANUAL_IP="n"
AUTOUPD="y"
UPDSEC="y"
HEADLESS="n"
QEMUAGENT="n"
SWARM_MASTER="n"
PORTAINER="n"
SWARM_KEY=""
CREATE_USER_CHOICE="y"
KUBERNETES_MASTER_TOKEN=""

# if no arguments are provided, return usage function
# maybe no need
# if [ $# -eq 0 ]; then
#     usage # run usage function
#     exit 1
# fi

usage () {
  echo "RHEL Docker + Kubernetes Post-Install usage"
  echo "----"
  echo "  Docker/Kubernetes Installer:"
  echo "    -do | --docker-only) -> Install Docker Only"
  echo "    -mk | --minikube   ) -> Install Minikube"
  echo "    -km | --kube-master) -> Install K3S Master"
  echo "    -kn | --kube-node  ) -> Install K3S Node"
  echo "----"
  echo "  Optional Parameters:"
  echo "     -u    | --user)              -> Specify non-root user to run the script"
  echo "     -a    | --gateway-address)   -> Specify a network gateway address to be used for firewalling purposes"
  echo "     -dd   | --docker-directory)  -> Specify a custom docker directory"
  echo "     -hs   | --hostname-change)   -> Specify a new machine hostname"
  echo "     -ni   | --network-interface) -> Specify the main Network Interface name"
  echo "     -fwd  | --enable-firewall)   -> Enable firewall (disabled by default)"
  echo "     -dhcp | --force-dhcp-renew)  -> Force DHCP refresh"
  echo "     -ipa  | --manual-ip)         -> Specify manual network addresses (IP/mask, Gateway, DNS)"
  echo "     -sp   | --change-ssh-port)   -> Change default SSH port"
  echo "     -nu   | --not-auto-update)   -> Do not install AUTOUPD and upgrade system with default method"
  echo "     -us   | --auto-sec-upd)      -> Automatically upgrade only security"
  echo "     -hd   | --headless)          -> Do not ask for reboot at the end (for automatic provisioners like Vagrant)"
  echo "     -qa   | --qemu-agent)        -> Install Quemu Agent drivers for KVM based Hypervisors"
  echo "     -pt   | --portainer)         -> Install Portainer Docker Manager"
  echo "     -pa   | --portainer-agent)   -> Install Portainer Docker Agent"
  echo "     -si   | --docker-swarm-init) -> Initialize a new Docker Swarm Master"
  echo "     -sj   | --docker-swarm-join) -> Provide a docker swarm key to connect this node to a swarm master"
  echo "----"
  echo "Version: 2.1.1"
  echo "Release Date: 28/11/2022"
  echo "Author: daniele.viti@commandware.com"
}

while [ "$1" != "" ]; do
    case $1 in
    -kn | --kube-node)
        shift
        SWARM_KEY=""
        SWARM_MASTER="n"
        KUBE_INSTALL="y"
        KUBE_NODE="y"
        if [[ $KUBE_MASTER == "y" || $KUBE_MASTER == "Y" ]]
        then
          echo "Kubernetes cannot be Master and Node at the same time!"
          exit 1
        fi
        ;;
    -km | --kube-master)
        SWARM_KEY=""
        SWARM_MASTER="n"
        KUBE_INSTALL="y"
        KUBE_MASTER="y"
        if [[ $KUBE_NODE == "y" || $KUBE_NODE == "Y" ]]
        then
          echo "Kubernetes cannot be Master and Node at the same time!"
          exit 1
        fi
        ;;
    -do | --docker-only)
        SWARM_KEY=""
        SWARM_MASTER="n"
        KUBE_INSTALL="n"
        KUBE_MASTER="n"
        ;;
    -mk | --minikube)
        SWARM_KEY=""
        SWARM_MASTER="n"
        KUBE_INSTALL="n"
        KUBE_MASTER="n"
        MINIKUBE="y"
        ;;
    -dd | --docker-directory)
        shift
        DOCKER_DIRECTORY=$1
        ;;
    -hs | --hostname-change)
        shift
        HOSTNAME=$1
        echo "$HOSTNAME" > /etc/hostname
        ;;
    -u | --user)
        shift
        ## Specify a manual Script user
        SCRIPT_USER=$1
        ## Script User Check
        if [[ $SCRIPT_USER == "" ]]
        then
          echo "No user defined"
          exit 1
        fi
        if id "$SCRIPT_USER" &>/dev/null; then
            USER_EXISTS="y"
        else
            USER_EXISTS="n"
        fi
        ;;
    -a | --gateway-address)
        shift
        ## Specify a manual SSH Gateway IP
        SSH_GW_IP=$1
        ;;
    -sj | --docker-swarm-join)
        shift
        ## Specify a docker swarm key
        SWARM_KEY=$1
        ;;
    -si | --docker-swarm-init)
        SWARM_MASTER="y"
        ;;
    -ni | --network-interface)
        shift
        NETINTERFACE=$1
        ;;
    -fwd | --enable-firewall)
        ENABLE_FIREWALL="y"
        ;;
    -dhcp | --force-dhcp-renew)
        DHCP_FORCE="y"
        ;;
    -ipa | --manual-ip)
        MANUAL_IP="y"
        ;;
    -sp | --change-ssh-port)
        SSH_CHANGE_PORT="y"
        ;;
    -nu | --not-auto-update)
        AUTOUPD="n"
        ;;
    -us | --auto-sec-upd)
        AUTOUPD="y"
        UPDSEC="y"
        ;;
    -hd | --headless)
        HEADLESS="y"
        ;;
    -qa | --qemu-agent)
        QEMUAGENT="y"
        ;;
    -pt | --portainer)
        PORTAINER="y"
        PORTAINER_AGENT="n"
        ;;
    -pa | --portainer-agent)
        PORTAINER="n"
        PORTAINER_AGENT="y"
        ;;
    -h | --help)
        usage # run usage function
        ;;
    *)
        usage
        exit 1
        ;;
    esac
    shift # remove the current value for '$1' and use the next
done

### START SCRIPT

sudo sed -i '/ swap / s/^/#/' /etc/fstab
sudo swapoff -a

### Force local time with NTP
sudo timedatectl set-ntp true
timedatectl
sudo dnf install chrony -y
sudo systemctl enable chronyd
sudo systemctl restart chronyd
date

### FORCE FIREWALL ON
sudo systemctl enable --now firewalld

### Increase max memory mapping
sudo sysctl -w vm.max_map_count=524288

### Install QUEMU Agent
if [[ $QEMUAGENT = "y" ]]
then
  sudo dnf install qemu-guest-agent -y
  sudo systemctl enable qemu-guest-agent
fi

## For Kubernetes the following link was references (adapted to Red Hat from Debian/Ubuntu)
## https://www.dgline.it/digitalroots/guida-per-la-costruzione-di-un-cluster-kubernetes-con-ubuntu/

## Check if ip and net interface are correct
if [[ $NETINTERFACE != "" ]]
then
  if [[ $(ip addr | grep $NETINTERFACE) == "" ]]
  then
    echo "Error: Network interface [$NETINTERFACE] not found."
    exit 1
  fi
  MACHINE_IP=$(ip -4 addr show $NETINTERFACE | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
  if [[ $MACHINE_IP == "" ]]
  then
    echo "IPv4 address not found for interface [$NETINTERFACE]"
    exit 1
  fi
fi

HOSTNAME=$(cat /etc/hostname)
echo "Hostname is set to: $HOSTNAME"

if [[ $SSH_GW_IP == "" ]]
then
  echo "You must enter the Gateway IP to allow connections"
  exit 1
fi

## Find network address from gateway
NETWORK_IP=$(awk -F"." '{print $1"."$2"."$3".2"}'<<<$SSH_GW_IP)

if [[ $SCRIPT_USER == "" ]]
then
  echo "You must enter a non root user as parameter"
	exit 1
fi

if id "$SCRIPT_USER" &>/dev/null; then
    USER_EXISTS="y"
else
    USER_EXISTS="n"
fi

## Replace current HOME variable with new one for script user selected
HOME="/home/$SCRIPT_USER"

## Adding the new user if not exists
if [[ $USER_EXISTS == "n" ]]
then
  if [[ $HEADLESS == "n" ]]
  then
    read -p "The User specified [$SCRIPT_USER] does not exists, should i create it? (N/y)" CREATE_USER_CHOICE
  else
    $CREATE_USER_CHOICE = "y"
  fi
  if [[ $CREATE_USER_CHOICE = "" || $CREATE_USER_CHOICE == "n" || $CREATE_USER_CHOICE == "N" ]]
  then
    echo "Error: User [$SCRIPT_USER] does not exists and you do not automatically want to create it. Aborting"
    exit 1
  fi
  echo "User $SCRIPT_USER does not exists, adding..."
  sudo adduser $SCRIPT_USER
  ## Create a new home and set permissions
  sudo mkdir "$HOME"
  sudo chown $SCRIPT_USER:$SCRIPT_USER "$HOME" -R
  sudo chmod u+rwx "$HOME" -R
  ## Add user to sudoers
  sudo usermod -aG sudo $SCRIPT_USER
fi

## System Utilities Install
sudo dnf install -y nano ca-certificates curl epel-release policycoreutils-python-utils dhclient
sudo dnf install -y fail2ban fail2ban-firewalld

if [[ $MANUAL_IP = "y" ]]
then
  read -p 'New IP (es: 192.168.100.10/24): ' MACHINE_IP
  read -p 'New Gateway: ' MACHINE_GATEWAY
  read -p 'New DNSs (DNS1,DNS2,DNS3...): ' MACHINE_DNS
  sudo nmcli connection modify $NETINTERFACE IPv4.address $MACHINE_IP
  sudo nmcli connection modify $NETINTERFACE IPv4.gateway $MACHINE_GATEWAY
  sudo nmcli connection modify $NETINTERFACE IPv4.dns $MACHINE_DNS,1.1.1.1
  sudo nmcli connection modify $NETINTERFACE IPv4.method manual
else
  sudo nmcli connection modify $NETINTERFACE IPv4.method auto
fi

## Security Prep
## SSH Config
if [[ $SSH_CHANGE_PORT = "y" ]]
then
  echo "Changing SSH port..."
  sudo sed -i "s/Port $OLD_SSH_PORT/Port $NEW_SSH_PORT/g" /etc/ssh/sshd_config
  sudo sed -i "s/PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config

  echo "Changing Firewall rules according to the new SSH Port"
  sudo firewall-cmd --permanent --zone=public --add-port=$NEW_SSH_PORT/tcp
  sudo firewall-cmd --permanent --zone=public --add-source=$NETWORK_IP/24
  sudo firewall-cmd --remove-service=ssh --zone=public --permanent
  sudo firewall-cmd --remove-service=cockpit --zone=public --permanent
  sudo firewall-cmd --remove-service=dhcpv6-client --zone=public --permanent

  echo "Setting Selinux for SSH Port..."
  sudo semanage port -a -t ssh_port_t -p tcp $NEW_SSH_PORT
else
  NEW_SSH_PORT="$OLD_SSH_PORT"
fi

if [[ $ENABLE_FIREWALL == "y" ]]
then
  if [[ $KUBE_INSTALL == "y" || $KUBE_INSTALL == "Y" ]]
  then
    sudo semanage port -a -t http_port_t -p tcp 8080
    sudo semanage port -a -t kubehc1_port_t -p tcp 6443
    sudo semanage port -a -t kubehc2_port_t -p tcp 10250
    sudo firewall-cmd --add-port=8080/tcp --permanent --zone=work
    sudo firewall-cmd --add-port=6443/tcp --permanent --zone=work
    sudo firewall-cmd --add-port=10248/tcp --permanent --zone=work
    sudo firewall-cmd --add-port=10250/tcp --permanent --zone=work
  fi
else
  echo "Warning! Firewall has been opened to public!"
  sudo firewall-cmd --permanent --zone=public --set-target=ACCEPT
fi

sudo systemctl enable --now fail2ban
sudo tee -a /etc/fail2ban/jail.conf > /dev/null <<EOT

[ssh]
enabled = true
port = $NEW_SSH_PORT
filter = sshd
logpath = /var/log/auth.log
EOT

## Forza DHCP
if [[ $DHCP_FORCE == "y" ]]
then
  dhclient -v
fi

## Install Kubernetes
if [[ $KUBE_INSTALL == "y" || $KUBE_INSTALL == "Y" ]]
then
  # Prepare master node and show token
  if [[ $KUBE_MASTER == "y" || $KUBE_MASTER == "Y" ]]
  then

    # Install K3S https://k3s.io/ with non root permissions
    curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644
    echo "Checking if k3s has been started up..."
    su - $SCRIPT_USER -c "k3s kubectl get node"
    su - $SCRIPT_USER -c "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml"
    su - $SCRIPT_USER -c 'echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> ~/.bashrc'
    chmod 600 /etc/rancher/k3s/k3s.yaml
    chown -R $SCRIPT_USER /etc/rancher
    echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> ~/.bashrc
    echo "Installing Helm.sh..."
    dnf install git tar -y
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    chmod 700 get_helm.sh
    ./get_helm.sh
    if [[ $PORTAINER = "y" ]]
    then
      echo "Installing Portainer..."
      su - $SCRIPT_USER -c "helm repo add portainer https://portainer.github.io/k8s/"
      su - $SCRIPT_USER -c "helm repo update"
      read -p "Install Portainer Business Edition? (y/N): " PT_BUSINESS
      PT_BE_BOOL="false"
      if [[ $PT_BUSINESS = "y" || $PT_BUSINESS = "Y" ]]
      then
        PT_BE_BOOL="true"
      fi
      su - $SCRIPT_USER -c "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml && helm upgrade --install --create-namespace -n portainer portainer portainer/portainer --set service.type=LoadBalancer --set enterpriseEdition.enabled=${PT_BE_BOOL}"
    fi
    echo "Use the following token to attach a new node to the cluster:"
    KUBERNETES_MASTER_TOKEN="$(cat /var/lib/rancher/k3s/server/node-token)"
  fi
  # Prepare node attaching to node
  if [[ $KUBE_NODE == "y" || $KUBE_NODE == "Y" ]]
  then
    read -p "Insert the k3s server address: " KUBE_SERVER
    read -p "Insert the k3s server token: " KUBE_TOKEN
    curl -sfL https://get.k3s.io | K3S_URL=https://${KUBE_SERVER}:6443 K3S_TOKEN=${KUBE_TOKEN} sh -
    su - $SCRIPT_USER -c "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml"
    su - $SCRIPT_USER -c 'echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> ~/.bashrc'
    echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> ~/.bashrc
  fi
  
else
  ## Standalone Docker Install
  sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  sudo dnf remove -y podman buildah
  sudo dnf install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
  sudo usermod -aG docker $SCRIPT_USER
  #curl -L "https://github.com/docker/compose/releases/download/v2.7.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  #sudo chmod +x /usr/local/bin/docker-compose
  ## Install Docker 2 Retro-Compatibility Command (Compose Switch)
  #sudo curl -fL https://raw.githubusercontent.com/docker/compose-cli/main/scripts/install/install_linux.sh | sh
  echo "--- Docker and Compose has been installed succesfully"

  ## Docker Post-Install
  sudo groupadd docker
  sudo usermod -aG docker $SCRIPT_USER
  sudo install -d -m 0755 -o $SCRIPT_USER -g docker "$DOCKER_DIRECTORY"
  sudo sed '/ExecStart=\/usr\/bin\/dockerd -H fd:\/\/ --containerd=\/run\/containerd\/containerd.sock/ s/$/ --exec-opt native.cgroupdriver=systemd/' /lib/systemd/system/docker.service
  sudo systemctl enable --now docker.service
  sudo systemctl enable --now containerd.service
  echo "--- Docker and Compose has been configured succesfully"

  # Install Minikube if requested
  if [[ $MINIKUBE = "y" ]]
  then
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
    echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
    chmod +x kubectl
    sudo mkdir -p /root/.local/bin
    sudo cp ./kubectl /root/.local/bin/kubectl
    mkdir -p ~/.local/bin
    mv ./kubectl ~/.local/bin/kubectl
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    sudo install minikube-linux-amd64 /usr/local/bin/minikube
    echo "Installing Minikube..."

    # Install as systemd service
    sudo cat << EOF > /etc/systemd/system/minikube.service
[Unit]
Description=Kickoff Minikube Cluster
After=docker.service

[Service]
Type=oneshot
ExecStart=/usr/local/bin/minikube start
RemainAfterExit=true
ExecStop=/usr/local/bin/minikube stop
StandardOutput=journal
User=$SCRIPT_USER
Group=docker

[Install]
WantedBy=multi-user.target
EOF
    su - $SCRIPT_USER -c "minikube start"
    sudo systemctl enable minikube

  else
    if [[ $SWARM_KEY != "" ]]
    then
      docker swarm join $SWARM_KEY
      ## cannot be master if already node
      $SWARM_MASTER = "n"
      ## Cannot install portainer if is not a swarm master
      $PORTAINER = "n"
    fi

    ## configure as master also if portainer is flagged for install
    if [[ $SWARM_MASTER = "y" || $PORTAINER = "y" ]]
    then
      docker swarm init --advertise-addr $MACHINE_IP
      echo "TAKE NOTE OF THIS SWARM KEY!!!"
      sleep 20
    fi
  fi

  # Portainer Master
  if [[ $PORTAINER = "y" ]]
  then
      docker run -d -p 8000:8000 -p 9443:9443 \
      --name portainer \
      --restart=always \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v portainer_data:/data portainer/portainer-ee:latest
  fi

  # Portainer Agent
  if [[ $PORTAINER_AGENT = "y" ]]
  then
    # If not swarm install standalone
    if [[ $SWARM_MASTER = "n" && $SWARM_KEY = "" ]]
    then
        docker run -d \
        -p 9001:9001 \
        --name portainer_agent \
        --restart=always \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v /var/lib/docker/volumes:/var/lib/docker/volumes \
        portainer/agent:latest
    else
       docker network create --driver overlay portainer_agent_network
       docker service create \
        --name portainer_agent \
        --network portainer_agent_network \
        -p 9001:9001/tcp \
        --mode global \
        --constraint 'node.role != worker' \
        --mount type=bind,src=//var/run/docker.sock,dst=/var/run/docker.sock \
        --mount type=bind,src=//var/lib/docker/volumes,dst=/var/lib/docker/volumes \
        portainer/agent:latest
    fi
  fi
fi

# Upgrade the entire system automatically
if [[ $AUTOUPD == "y" ]]
then
  echo "Found AUTOUPD for security system upgrade"
  sudo dnf install dnf-automatic -y
  if [[ $UPDSEC == "y" ]]
  then
    sed -i "s/upgrade_type = default/upgrade_type = security/g" /etc/dnf/automatic.conf
  fi
  sed -i "s/download_updates = no/download_updates = yes/g" /etc/dnf/automatic.conf
  sed -i "s/apply_updates = no/apply_updates = yes/g" /etc/dnf/automatic.conf
  sudo systemctl enable --now dnf-automatic.timer
  #docker run --detach --name watchtower -e WATCHTOWER_SCHEDULE="0 0 5 * * *" --restart unless-stopped --volume /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower
else
  echo "Running standard dnf upgrade"
  sudo dnf update -y
  sudo dnf autoremove -y
fi

if [[ $HEADLESS = "n" ]]
then
  NEW_SCRIPT_USER_PASSWORD=$(tr -cd "[:alnum:]" < /dev/urandom | fold -w32 | head -n 1)
  NEW_ROOT_PASSWORD=$(tr -cd "[:alnum:]" < /dev/urandom | fold -w32 | head -n 1)

  ## User Output
  echo "Setting secure password for user $SCRIPT_USER"
  echo "$SCRIPT_USER:$NEW_SCRIPT_USER_PASSWORD"|chpasswd
  echo "Setting secure password for user root"
  echo "root:$NEW_ROOT_PASSWORD"|chpasswd

  echo "-"
  echo "-"
  echo "-"
  echo "Script end - User Information"

  ## User Output
  if [[ $MANUAL_IP = "y" ]]
  then
    echo "Warning! Network has been manually configured:"
    echo "- IP Address is: $MACHINE_IP"
    echo "- Gateway Address is: $MACHINE_GATEWAY"
    echo "- DNSs are: $MACHINE_DNS"
  fi

  if [[ $KUBERNETES_MASTER_TOKEN != "" ]]
  then
    echo "Kubernetes Master Address: ${MACHINE_IP}:6443"
    echo "Kubernetes Master Token: ${KUBERNETES_MASTER_TOKEN}"
  fi

  if [[ $HEADLESS = "n" ]]
  then
    echo "New $SCRIPT_USER password is: $NEW_SCRIPT_USER_PASSWORD"
    echo "New root password is: $NEW_ROOT_PASSWORD"
    echo "Connect via SSH: ssh -p $NEW_SSH_PORT $SCRIPT_USER@$MACHINE_IP"
    echo "Warning! System will be restarted, please save all the info before"
    read -p "Press [Enter] to reboot system"
    reboot
  fi
fi

echo "Automatic provisioning completed, you may want to restart bash session"
