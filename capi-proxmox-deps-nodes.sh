#!/bin/bash

# Using curl
# curl -sSL https://gitlab.com/dnviti/Bash-Scripts/raw/master/capi-proxmox-deps-nodes.sh | bash
# Using wget
# wget -O - https://gitlab.com/dnviti/Bash-Scripts/raw/master/capi-proxmox-deps-nodes.sh | bash

echo "Installing dependencies..."
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://pkgs.k8s.io/core:/stable:/v1.27/rpm/
enabled=1
gpgcheck=1
gpgkey=https://pkgs.k8s.io/core:/stable:/v1.27/rpm/repodata/repomd.xml.key
exclude=kubelet kubeadm kubectl cri-tools kubernetes-cni
EOF
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y kubelet kubeadm kubectl containerd.io iproute-tc --disableexcludes=kubernetes
sudo systemctl enable --now kubelet
sudo systemctl enable --now containerd

sudo cloud-init clean
sudo rm -rf /var/lib/cloud/*
sudo rm -rf /var/log/cloud-init.log
sudo truncate -s 0 /etc/machine-id

# Finally, initialize the management cluster
echo "Dependencies installed! Shutting Down..."
