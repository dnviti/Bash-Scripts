#!/bin/bash

# First install docker and reboot
# sudo su -c "bash <(wget -qO- https://gitlab.com/dnviti/Bash-Scripts/raw/master/almalinux-postinstall.sh) -do" root

# Using curl
# curl -sSL https://gitlab.com/dnviti/Bash-Scripts/raw/master/capi-proxmox-deps.sh | bash
# Using wget
# wget -O - https://gitlab.com/dnviti/Bash-Scripts/raw/master/capi-proxmox-deps.sh | bash


echo "Checking docker..."
if systemctl is-active --quiet docker; then
    echo "Docker is running."
else
    echo "Docker is not running. Please install docker rootless first"
    exit 1
fi

docker ps > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "docker ps command failed. Checking for permission issues..."

    if [ ! -e /var/run/docker.sock ]; then
        echo "Docker socket does not exist."
        exit 1
    elif [ -w /var/run/docker.sock ]; then
        echo "Docker socket exists and current user has write permission."
    else
        echo "Permission issue: Current user does not have write permission on Docker socket."
        exit 1
    fi
else
    echo "docker ps command succeeded."
fi

echo "Installing kind..."
[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64
sudo install -o root -g root -m 0755 kind /usr/local/bin/kind
rm -f kind
source "$HOME/.bashrc"
kind create cluster

echo "Installing kubeadm..."
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://pkgs.k8s.io/core:/stable:/v1.29/rpm/
enabled=1
gpgcheck=1
gpgkey=https://pkgs.k8s.io/core:/stable:/v1.29/rpm/repodata/repomd.xml.key
exclude=kubelet kubeadm kubectl cri-tools kubernetes-cni
EOF
sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
sudo systemctl enable --now kubelet

echo "Installing kubectl..."
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm -f kubectl
source "$HOME/.bashrc"
kubectl cluster-info

echo "Installing helm..."
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
rm -f get_helm.sh
source "$HOME/.bashrc"

curl -L https://github.com/kubernetes-sigs/cluster-api/releases/download/v1.6.1/clusterctl-linux-amd64 -o clusterctl
sudo install -o root -g root -m 0755 clusterctl /usr/local/bin/clusterctl
rm -f clusterctl
source "$HOME/.bashrc"
clusterctl version

echo "Configure clusterctl..."
mkdir -p ~/.cluster-api
cat << EOF > ~/.cluster-api/clusterctl.yaml
providers:
  - name: in-cluster
    url: https://github.com/kubernetes-sigs/cluster-api-ipam-provider-in-cluster/releases/latest/ipam-components.yaml
    type: IPAMProvider
EOF

# Finally, initialize the management cluster
echo "Dependencies installed!"
echo "Now you shoud open $HOME/.cluster-api/clusterctl.yaml file and add your variables to configure installation"
echo "See: https://github.com/ionos-cloud/cluster-api-provider-proxmox/blob/main/docs/Usage.md"
echo "Configuration finished, now you can run 'clusterctl init --infrastructure proxmox --ipam in-cluster'"
