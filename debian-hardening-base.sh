#!/bin/bash

# Thanks to headintheclouds and DViti for part of the script
#set -o xtrace

if [[ $USER != "root" ]]
then
    echo "Script must be run as root"
    exit 1
fi

sed -i "s/#Port/Port/g" /etc/ssh/sshd_config
S=$(grep "Port " /etc/ssh/sshd_config)
OLD_SSH_PORT=${S:5:99}
NEW_SSH_PORT=$(( $RANDOM % 999 + 22000 ))
SSH_GW_IP=$(ip route | grep default | awk '{print $3}' | awk 'END{print}')
## Use the first er user available post-install
SCRIPT_USER=$(id -nu 1000)
MACHINE_IP=$(ip route | grep default | awk '{print $9}' | awk 'END{print}')



HOSTNAME=$(cat /etc/hostname)
echo "Hostname is set to: $HOSTNAME"


## System Utilities Install
apt install -y fail2ban nano wget iptables-persistent

## Security Prep


## Secure SSH
  echo -e "\e[33mSecuring SSH\e[0m"
  echo "Changing SSH port"
  sed -i "s/Port $OLD_SSH_PORT/Port $NEW_SSH_PORT/g" /etc/ssh/sshd_config
  sed -i "s/PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config
  echo

  echo "Changing Firewall rules according to the new SSH Port"
  iptables -A INPUT -p tcp --dport $NEW_SSH_PORT -j ACCEPT
  iptables-save > /etc/iptables/rules.v4



echo -e "\e[33mConfiguring fail2ban\e[0m"
tee -a /etc/fail2ban/jail.conf > /dev/null <<EOT

[ssh]
enabled = true
port = $NEW_SSH_PORT
filter = sshd
logpath = /var/log/auth.log
EOT

systemctl enable --now fail2ban
echo


# Monitor user activities
echo -e "\e[33mMonitoring user activities\e[0m"
echo "Installing auditd for user activity monitoring..."
 apt-get install auditd -y
echo "Configuring auditd..."
 echo "-w /var/log/auth.log -p wa -k authentication" |  tee -a /etc/audit/rules.d/audit.rules
 echo "-w /etc/passwd -p wa -k password-file" |  tee -a /etc/audit/rules.d/audit.rules
 echo "-w /etc/group -p wa -k group-file" |  tee -a /etc/audit/rules.d/audit.rules
 systemctl enable auditd
echo ""

echo -e "\e[33mInstalling and running Rootkit detection...\e[0m"
 apt-get install -y rkhunter
 rkhunter --update
 rkhunter --propupd
 rkhunter --check
echo


# Monitor system logs
echo -e "\e[33mMonitoring system logs\e[0m"
echo "Installing logwatch for system log monitoring..."
 apt-get install logwatch -y
echo ""


echo -e "\e[33mConfiguring autoupdate\e[0m"
  apt install -y unattended-upgrades
  sed -i "s/upgrade_type = default/upgrade_type = security/g" /etc/apt/apt.conf.d/20auto-upgrades
  sed -i "s/Unattended-Upgrade::Download-Upgradeable-Packages \"0\";/Unattended-Upgrade::Download-Upgradeable-Packages \"1\";/g" /etc/apt/apt.conf.d/50unattended-upgrades
  sed -i "s/Unattended-Upgrade::Auto-Upgrade \"0\";/Unattended-Upgrade::Auto-Upgrade \"1\";/g" /etc/apt/apt.conf.d/50unattended-upgrades
  systemctl enable --now unattended-upgrades



  NEW_SCRIPT_USER_PASSWORD=$(tr -cd "[:alnum:]" < /dev/urandom | fold -w32 | head -n 1)
  NEW_ROOT_PASSWORD=$(tr -cd "[:alnum:]" < /dev/urandom | fold -w32 | head -n 1)

  ## User Output
  echo "Setting secure password for user $SCRIPT_USER"
  echo "$SCRIPT_USER:$NEW_SCRIPT_USER_PASSWORD"|chpasswd
  echo "Setting secure password for user root"
  echo "root:$NEW_ROOT_PASSWORD"|chpasswd

  echo "-"
  echo "-"
  echo "-"
  echo "Script end - User Information"


    echo "New $SCRIPT_USER password is: $NEW_SCRIPT_USER_PASSWORD"
    echo "New root password is: $NEW_ROOT_PASSWORD"
    echo "Connect via SSH: ssh -p $NEW_SSH_PORT $SCRIPT_USER@$MACHINE_IP"
    echo "Warning! System will be restarted, please save all the info before"
    read -p "Press [Enter] to reboot system"
    reboot


echo "Automatic provisioning completed, you may want to restart bash session"