#!/bin/bash

usage () {
  echo ".NET NuGet Package Version Control usage"
  echo "----"
  echo "  NuGet Package Version Control:"
  echo "    -f | --proj-file) -> Specify Project File to be parsed (default *.csproj)"
  echo "    -r | --recursive) -> Search recursively on sub-directories"
  echo "Version: 1.0.0"
  echo "Release Date: 11/11/2022"
  echo "Author: daniele.viti@commandware.com"
}

update_ver () {
    if [[ "$1" != *".csproj"* ]]; then
        echo "ERROR: File $1 is not a valid Project file"
        return
    fi

    if [[ ! -f "$1" ]]
    then
        echo "ERROR: File $1 could not be found"
        return
    fi

    echo "OK: Found Project File $1"

    ### Get text between <Version>*</Version>, remove trailing spaces and non numeric characters
    NUGET_OLD_VERSION=$(awk '/<Version>/,/<\/Version>/' "$1" | sed 's/<Version>//g' | sed 's/<\/Version>//g' | xargs echo -n | sed 's/[^0-9]*//g')

    if [[ $NUGET_OLD_VERSION != "" ]]
    then
        echo "OK: Found Current Version $NUGET_OLD_VERSION"
    else
        echo "WARN: Project has no Current Version... Ignoring"
        return
    fi

    ### Split string by .
    IFS='.'
    read -a NUGET_OLD_VERSION_PARTS <<< $NUGET_OLD_VERSION
    ###

    NUGET_OLD_VERSION_YEAR=${NUGET_OLD_VERSION_PARTS[0]}
    NUGET_OLD_VERSION_MONTH=${NUGET_OLD_VERSION_PARTS[1]}
    NUGET_OLD_VERSION_SEQ=${NUGET_OLD_VERSION_PARTS[2]}

    DATE_NOW=$(date +%y.%m)
    ### Split string by .
    IFS='.'
    read -a NUGET_NEW_VERSION_PARTS <<< $DATE_NOW
    ###

    NUGET_NEW_VERSION_YEAR=${NUGET_NEW_VERSION_PARTS[0]}
    NUGET_NEW_VERSION_MONTH=${NUGET_NEW_VERSION_PARTS[1]}

    if [[ $NUGET_NEW_VERSION_YEAR == $NUGET_OLD_VERSION_YEAR && $NUGET_NEW_VERSION_MONTH == $NUGET_OLD_VERSION_MONTH ]]
    then
        ### If new version on same day add +1 to seq number
        NUGET_NEW_VERSION_SEQ=$((NUGET_OLD_VERSION_SEQ+1))
    else
        ### If today is a new day set a new version and reset seq number
        NUGET_NEW_VERSION_SEQ=0
    fi

    NUGET_NEW_VERSION="${NUGET_NEW_VERSION_YEAR}.${NUGET_NEW_VERSION_MONTH}.${NUGET_NEW_VERSION_SEQ}"

    if [[ $NUGET_NEW_VERSION != "" ]]
    then
        echo "OK: Upgrading to New Version $NUGET_NEW_VERSION"
    fi

    ### Physically Replace Version String
    sed -i "s/<Version>$NUGET_OLD_VERSION<\/Version>/<Version>$NUGET_NEW_VERSION<\/Version>/g" "$1"

    if [[ $NUGET_NEW_VERSION != "" ]]
    then
        echo "OK: Package Upgraded from version $NUGET_OLD_VERSION to version $NUGET_NEW_VERSION"
    fi
}

## Default Parameters
PROJ_FILE_NAME="*.csproj"
RECURSIVE="false"

while [ "$1" != "" ]; do
    case $1 in
    -f | --proj-file)
        shift
        PROJ_FILE_NAME=$1
        ;;
    -r | --recursive)
        RECURSIVE="true"
        ;;
    -h | --help)
        usage # run usage function
        ;;
    *)
        usage
        exit 1
        ;;
    esac
    shift # remove the current value for '$1' and use the next
done

echo "START"

if [[ $RECURSIVE == "true" ]]
then
    echo "OK: Recursive Mode"

    shopt -s globstar
    for proj in **/$PROJ_FILE_NAME;
    do
        echo "------- $proj"
        update_ver "$proj"
        echo "-------"
    done
else
    echo "OK: Single Mode"
    update_ver $PROJ_FILE_NAME
fi

echo "END"