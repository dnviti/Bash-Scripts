#!/bin/bash

read -p "Enter the Kubernetes version: " KUBE_VERSION

echo "Kubernetes version set to: $KUBE_VERSION"

sudo apt remove needrestart -y
sudo apt upgrade -y
sudo cp /etc/sudoers /tmp/sudoers.bak && sudo sed -i '/^%sudo\s*ALL=(ALL:ALL) ALL$/c\%sudo   ALL=(ALL:ALL) NOPASSWD:ALL' /tmp/sudoers.bak && sudo visudo -c -f /tmp/sudoers.bak && sudo cp /tmp/sudoers.bak /etc/sudoers

sudo apt install ca-certificates apt-transport-https curl nano -y

sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
sudo usermod -aG docker $USER
sudo systemctl enable containerd --now
sudo systemctl enable docker --now
curl -fsSL https://pkgs.k8s.io/core:/stable:/v$KUBE_VERSION/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v$KUBE_VERSION/deb/ /" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt update
sudo apt install -y kubeadm kubectl kubelet kubernetes-cni iproute2
sudo apt-mark hold kubelet kubeadm kubectl
sudo systemctl enable --now kubelet
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh && rm -f get_helm.sh
sudo swapoff -a
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab > /dev/null
echo -e "overlay\nbr_netfilter" | sudo tee -a /etc/modules-load.d/containerd.conf > /dev/null
sudo modprobe overlay
sudo modprobe br_netfilter
echo -e "net.bridge.bridge-nf-call-ip6tables = 1\nnet.bridge.bridge-nf-call-iptables = 1\nnet.ipv4.ip_forward = 1" | sudo tee -a /etc/sysctl.d/kubernetes.conf > /dev/null

read -p "Is this master or worker? (MASTER/WORKER) " KUBE_NODE_TYPE

echo "You selected $KUBE_NODE_TYPE k8s node type!"

if [[ $KUBE_NODE_TYPE == "MASTER" ]]; then
    echo 'KUBELET_EXTRA_ARGS="--cgroup-driver=cgroupfs"' | sudo tee -a /etc/default/kubelet > /dev/null
    sudo systemctl daemon-reload && sudo systemctl restart kubelet
    echo -e '{\n    "exec-opts": [\n        "native.cgroupdriver=systemd"\n    ],\n    "log-driver": "json-file",\n    "log-opts": {\n        "max-size": "100m"\n    },\n    "storage-driver": "overlay2"\n}' | sudo tee /etc/docker/daemon.json > /dev/null
    sudo systemctl daemon-reload && sudo systemctl restart docker
    echo 'Environment="KUBELET_EXTRA_ARGS=--fail-swap-on=false"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf > /dev/null
    sudo systemctl daemon-reload && sudo systemctl restart kubelet
    sudo rm /etc/containerd/config.toml
    sudo systemctl restart containerd
    sudo kubeadm config images pull
    sudo kubeadm reset -f
    sudo rm -fr /etc/kubernetes/; sudo rm -fr ~/.kube/; sudo rm -fr /var/lib/etcd; sudo rm -rf /var/lib/cni/
    sudo systemctl daemon-reload
    sudo iptables -F && sudo iptables -t nat -F && sudo iptables -t mangle -F && sudo iptables -X
    docker rm -f `docker ps -a | grep "k8s_" | awk '{print $1}'`
    rm -f /etc/kubernetes/manifests/kube-apiserver.yaml
    rm -f /etc/kubernetes/manifests/kube-controller-manager.yaml
    rm -f /etc/kubernetes/manifests/kube-scheduler.yaml
    rm -f /etc/kubernetes/manifests/etcd.yaml
    rm -f /var/lib/kubelet/config.yaml
    rm -rf /var/lib/etcd
    sudo kubeadm init
    # if non root user
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
elif [[ $KUBE_NODE_TYPE == "WORKER" ]]; then
    echo "not implemented"
else
    echo "Error: KUBE_NODE_TYPE must be either 'MASTER' or 'WORKER'."
fi
