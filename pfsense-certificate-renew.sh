#!/bin/bash

## Automatic ACME Certificate renew for pfSense
/usr/local/pkg/acme/acme.sh  --issue  --domain 'commandware.com' --dns 'dns_ovh'  --domain '*.commandware.com' --dns 'dns_ovh'  --home '/tmp/acme/CommandWare-Wildcard/' --accountconf '/tmp/acme/CommandWare-Wildcard/accountconf.conf' --force --reloadCmd '/tmp/acme/CommandWare-Wildcard/reloadcmd.sh' --log-level 3 --log '/tmp/acme/CommandWare-Wildcard/acme_issuecert.log'

# CONFIG FILE ROWS EXAMPLE (nginx-proxy-data.txt)
# One config per row!
## server-url---------------|user_email-------|password|certificate_id
## http(s)://ip-address:port|admin@example.com|changeme|1

### Import Certificates to nginx proxy manager using api
IFS=$'\n' read -d '' -r -a NGINXDATA < nginx-proxy-data.txt

for datarow in "${NGINXDATA[@]}"
do
    :
    IFS=$'|' read -a datacol <<< $datarow
    ### ${datacol[0]} # Base Url without final /
    ### ${datacol[1]} # identity
    ### ${datacol[2]} # secret

    DATA_RAW="{
        \"identity\":\"${datacol[1]}\",
        \"secret\":\"${datacol[2]}\"
    }"

    ## Get Token
    CONTENT=$(curl --location --request POST "${datacol[0]}/api/tokens" \
    --header 'Content-Type: application/json' \
    --data-raw "{
        \"identity\":\"${datacol[1]}\",
        \"secret\":\"${datacol[2]}\"
    }")

    echo $CONTENT

    TOKEN=$(jq -r '.token' <<< $CONTENT)

    ## Upload Replace Certificate
    curl --location --request POST "${datacol[0]}/api/nginx/certificates/${datacol[3]}/upload" \
    --header "Referer: ${datacol[0]}/api/nginx/certificates" \
    --header "Authorization: Bearer $TOKEN" \
    --form 'certificate=@"/tmp/acme/CommandWare-Wildcard/commandware.com/fullchain.cer"' \
    --form 'certificate_key=@"/tmp/acme/CommandWare-Wildcard/commandware.com/commandware.com.key"'
done