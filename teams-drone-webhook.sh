#!/bin/bash

usage () {
  echo "Teams Webhook Message Sender"
  echo "----"
  echo "  Docker/Kubernetes Installer:"
  echo "    -ur | --url)         -> Teams Webhook Url"
  echo "    -cl | --color)       -> Specify Message Color"
  echo "    -ti | --title)       -> Card Title"
  echo "    -st | --sub-title)   -> Card Subtitle"
  echo "    -du | --deploy-url)  -> Package or Service url"
  echo "    -an | --app-name)    -> Name of library or application"
  echo "----"
  echo "  Optional Parameters:"
  echo "----"
  echo "Version: 1.0.0"
  echo "Release Date: 21/10/2022"
  echo "Author: daniele.viti@commandware.com"
}

while [ "$1" != "" ]; do
    case $1 in
    -ur | --url)
		shift
        URL=$1
        ;;
    -cl | --color)
        shift
        COLOR=$1
        ;;
    -ti | --title)
        shift
        TITLE=$1
        ;;
    -st | --sub-title)
        shift
        SUBTITLE=$1
        ;;
    -du | --deploy-url)
        shift
        DEPLOYURL=$1
        ;;
    -an | --app-name)
        shift
        APPNAME=$1
        ;;
    -h | --help)
        usage # run usage function
        exit 0
        ;;
    *)
        usage
        exit 1
        ;;
    esac
    shift # remove the current value for '$1' and use the next
done

curl --location --request POST "$URL" \
--header "Content-Type: application/json" \
--data-raw "{
    \"@type\": \"MessageCard\",
    \"@context\": \"http://schema.org/extensions\",
    \"themeColor\": \"$COLOR\",
    \"summary\": \"$TITLE\",
    \"sections\": [{
        \"activityTitle\": \"$TITLE\",
        \"activitySubtitle\": \"$SUBTITLE\",
        \"activityImage\": \"\",
        \"facts\": [{
            \"name\": \"URL\",
            \"value\": \"[$APPNAME]($DEPLOYURL)\"
        },
        {
            \"name\": \"Application\",
            \"value\": \"$APPNAME\"
        }],
        \"markdown\": true
    }]
}"